# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# History
HISTSIZE=5000               # How many lines of history to keep in memory
HISTFILE=~/.zsh_history     # Where to save history to disk
SAVEHIST=5000               # Number of history entries to save to disk
setopt appendhistory        # Append history to the history file (no overwriting)
setopt sharehistory         # Share history across terminals
setopt incappendhistory     # Immediately append to the history file, not just when a term is killed

# Aliases
alias doas="doas " # This is required for most of the aliases that need superuser access
alias l="ls -lah --color=auto"
alias irssi="irssi --config=~/.config/irssi/customconfig --home=~/.config/irssi"
alias gs="git status"
alias gd="git diff"
alias gdc="git diff --cached"
alias gl="git lg"
alias ga="git add"
alias gap="git add --patch"
alias gcm="git commit --message"
alias rcs="rc-service"
alias rcu="rc-update"
alias doco="docker-compose"
alias erepo="eselect repository"
alias tlmgr="/usr/share/texmf-dist/scripts/texlive/tlmgr.pl --usermode"
alias bat="bat --theme Nord"

# This is temporary because startx does't respect $XINITRC for some reason
alias startx="startx $XDG_CONFIG_HOME/x/.xinitrc"

# Settings
unsetopt beep # Don't beep
bindkey -e    # Emacs keymap

# Completion
autoload -Uz compinit # Load completions
compinit              # Start completions

# Stop backward-kill-word on directory delimiter
autoload -U select-word-style
select-word-style bash

# Start p10k and source config
source $XDG_CONFIG_HOME/zsh/powerlevel10k/powerlevel10k.zsh-theme
[[ ! -f ~/.config/p10k/.p10k.zsh ]] || source ~/.config/p10k/.p10k.zsh

# Load nvm
export NVM_DIR="$HOME/.config/nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"

# Re-source .zshenv so $PATH is set correctly
# Necessary because zsh has a weird sourcing order for startup files
source $HOME/.zshenv

#cat $XDG_CONFIG_HOME/zsh/welcome

# https://gitlab.com/dwt1/shell-color-scripts
[ -x "$(command -v colorscript)" ] && colorscript random

# pnpm
export PNPM_HOME="/home/nils/.local/share/pnpm"
export PATH="$PNPM_HOME:$PATH"
# pnpm end
