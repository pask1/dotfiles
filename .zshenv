export SHELL="zsh"
export EDITOR="nvim"
export BROWSER="firefox"

# Settings (maybe this should be in zshrc)
export LESS="$LESS --mouse --wheel-lines=3"

# ~/ clean-up
export XDG_CONFIG_HOME="$HOME/.config"
export ZDOTDIR="${XDG_CONFIG_HOME:-$HOME/.config}/zsh"
export XINITRC="$XDG_CONFIG_HOME/x/.xinitrc"
export WEECHAT_HOME="${XDG_CONFIG_HOME:-$HOME/.config}/weechat"
export MANPATH="$MANPATH:/usr/local/texlive/2020/texmf-dist/doc/man"
export INFOPATH="$INFOPATH:/usr/local/texlive/2020/texmf-dist/doc/info"
export PATH=$HOME/.yarn/bin:/usr/local/texlive/2020/bin/x86_64-linux:$PATH
export PATH=/usr/local/texlive/2020/bin/x86_64-linux:$PATH
export PATH=$HOME/.local/bin:$PATH
export SPICETIFY_INSTALL=$HOME/.spicetify
export PATH=$PATH:$HOME/.spicetify
export PATH=$PATH:$HOME/.cargo/bin

# Uncomment these lines to set MYVIMRC and VIMINIT, for vim
# Apparently Neovim also uses these if you set them, but I need Neovim to use .config/nvim/init.vim
#export MYVIMRC="${XDG_CONFIG_HOME:-$HOME/.config}/vim/.vimrc"
#export VIMINIT="source $MYVIMRC"
