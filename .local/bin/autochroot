#!/bin/bash

# autochroot
# 
# !!!-- THIS SCRIPT IS A WORK IN PROGRESS. CAVEAT EMPTOR --!!!
# 
# Automated script to chroot into gentoo installation from (live|admin)(usb|cd)
# Credit: StevenC21 on the gentoo forums
# Source: https://forums.gentoo.org/viewtopic-t-1091406-start-0.html
# 
# Modifications have been done by pask and Spawns_Carpeting

set -eu

help() {
	cat - >&2 <<EOF
autochroot - automatically chroot into gentoo environment

Usage:
autochroot NEWROOT SHELL

NEWROOT specifies which directory you would like to chroot into, SHELL
specifies which shell you would like to use.

This script has been designed to work best with bash, as this is what the hand-
book mainly uses. When invoked properly, it will chroot into the new
environment, source /etc/profile, and set a prompt that indicates that we're
now in a chrooted environment.
EOF
}

case "$1" in --help|-h) help; exit 0;; esac

FS=$1
SH=$2

if [[ -z ${FS} ]]; then
        echo "Filesystem to chroot not set"
        exit 1
fi

if [[ -z ${SH} ]]; then
        echo "Shell to chroot with not set"
        exit 1
fi

mkdir -p ${FS}/tmp/gntmnt/

if [[ ! -e ${FS}/tmp/gntmnt/*.gntmnt ]]; then # If nobody is in the system then we should mount the needed filesystems
        touch ${FS}/tmp/gntmnt/$$.gntmnt # Create a temporary lockfile to tell the script that we are inside the system
        echo "No Mount detected, Mounting Necessary Filesystems..."
        mount --types proc /proc ${FS}/proc
        mount --rbind /sys ${FS}/sys
        mount --make-rslave ${FS}/sys
        mount --rbind /dev ${FS}/dev
        mount --make-rslave ${FS}/dev
fi

# Chroot, source /etc/profile, and set prompt to indicate chroot
echo "Chrooting into Gentoo system..."
sudo chroot "${FS}" "${SH}" -c "/bin/bash --rcfile <(echo 'echo Sourcing /etc/profile...; source /etc/profile; export PS1=\"(chroot) \${PS1}\"')"

# By this point the user has exitted from the Chroot, its time to unmount filesystems - assuming this is the only remaining shell.
rm ${FS}/tmp/gntmnt/$$.gntmnt

if [[ ! -e ${FS}/tmp/gntmnt/*.gntmnt ]]; then
        echo "Unmounting Filesystems..."
        umount -l ${FS}/dev{/shm,/pts,}
else
        echo "Other shells chrooted, NOT unmounting filesystems..."
fi 
